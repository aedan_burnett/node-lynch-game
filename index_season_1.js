/* NARRATOR BOT WOOOT */

// Global Block
var GAME_ACTIVE = false;
var ACTIVE_CHANNEL = 'the-town';

var SPECIALS = ["GHOUL", "GHOUL", "LICH", "MASON", "MASON", "SEER"];
var SCHEDULER;

var express = require('express');
var request = require('request');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
var moment = require("moment");
var schedule = require("node-schedule");
var app = express();

// These are all defined in a heroku environment variable.
var clientId = process.env.CLIENT_ID;
var clientSecret = process.env.CLIENT_SECRET;
var mongouri = process.env.MONGOLAB_URI;
var db = mongoose.connect(mongouri);

var playerSchema = mongoose.Schema({
    name: {type: String, required: true},
    role: {type: String, required: true},
    isDead: {type: Boolean, required: true},
    usedAbility: {type: Boolean, required: true},
});
var Player = mongoose.model('Player', playerSchema);

var lynchSchema = mongoose.Schema({
    voter: {type: String, required: true},
    target: {type: String},
});
var Lynch = mongoose.model('Lynch', lynchSchema);

var feastSchema = mongoose.Schema({
    ghoul: {type: String, required: true},
    victim: {type: String},
});
var Feast = mongoose.model('Feast', feastSchema);


app.set('port', (process.env.PORT || 5000));

app.use(express.static(__dirname + '/public'));
app.use( bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

// views is directory for all template files
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');

// Simple get, good for testing you launched correctly.
app.get('/', function(request, response) {
  response.send('Narrator Bot Heroku is working! Path Hit: ' + request.url);
});

app.get('/oauth', function(req, res) {
    // When a user authorizes an app, a code query parameter is passed on the oAuth endpoint. If that code is not there, we respond with an error message
    if (!req.query.code) {
        res.status(500);
        res.send({"Error": "Looks like we're not getting code."});
        console.log("Looks like we're not getting code.");
    } else {
        // If it's there...

        // We'll do a GET call to Slack's `oauth.access` endpoint, passing our app's client ID, client secret, and the code we just got as query parameters.
        request({
            url: process.env.OAUTH, //URL to hit
            qs: {code: req.query.code, client_id: clientId, client_secret: clientSecret}, //Query string data
            method: 'GET', //Specify the method

        }, function (error, response, body) {
            if (error) {
                console.log(error);
            } else {
                res.json(body);

            }
        })
    }
});

app.listen(app.get('port'), function() {
  console.log('Node app is running on port', app.get('port'));
});

app.post('/testdata', function(req, res) {
  var user = req.body['user_name'];
  var channel = req.body['channel_name'];
  if (channel != ACTIVE_CHANNEL) {
    res.send("Please perform this action in #"+ACTIVE_CHANNEL+".");
    return;
  }
  if (user != 'aedan.burnett') {
    res.send("Only @aedan.burnett can do this.");
    return;
  }
  if (GAME_ACTIVE == true) {
    res.send("Game has started, don't add test data now.");
    return;
  }
  res.send('Test data populating . . .');
  var dummy;
  for (var i = 0 ; i < 15 ; ++i) {
    var dummy = new Player({name: '@test'+i, role: 'VILLAGER', isDead : false, usedAbility : false});
    dummy.save({upsert: true}, function (err, item) {
      if (err) return console.error(err);
    });
  }
});

app.post('/commands', function(req, res) {
  res.send('All commands must be used in #'+ACTIVE_CHANNEL+'\n'+
  '```EVERYONE\n'+
  '/townsignup - Join the game.\n'+
  '/townmembers - See the list of players and if they are alive or dead.\n'+
  '/townrole - Learn what role you have been assigned.\n'+
  '/townlynch @victim - Publically vote to lynch @victim. You may abstain by not specifying a victim.\n'+
  '/townvotes - See the list of current lynch votes.\n\n'+

  'MASONS\n'+
  '/townmasons - Privately tells you who the other MASONS are. You can trust them.\n\n'+

  'GHOULS & LICHS\n'+
  '/townghouls - Privately tells you who the other GHOUL faction players are.\n'+
  '/townfeast @victim - Privately vote to feast on @victim.\n\n'+

  'SEERS & LICHS\n'+
  '/townreveal @target - Privately tells you the role of @target. Limit once per day.```');
});

app.post('/announce', function(req, res) {
  var text = req.body['text'];
  var user = req.body['user_name'];
  var channel = req.body['channel_name'];
  if (channel != ACTIVE_CHANNEL) {
    res.send("Please perform this action in #"+ACTIVE_CHANNEL+".");
    return;
  }

  // NO ANNOUNCING SHIT UNLESS YOU ARE ME
  if (user != 'aedan.burnett') {
    res.send("You recall that only @aedan.burnett had permission to make announcements as the Narrator.");
    return;
  } else {
    res.send("Preparing announcement. . .");
    slackpost(text);
  }
});

app.post('/signup', function(req, res) {
  var user = req.body['user_name'];
  var channel = req.body['channel_name'];
  if (channel != ACTIVE_CHANNEL) {
    res.send("Please perform this action in #"+ACTIVE_CHANNEL+".");
    return;
  } else if (GAME_ACTIVE) {
    res.send("You regrettably realize the story has begun and you cannot take part. You will have to try again some other time.");
    return;
  }

  var playercount = 0;
  Player.find().exec(function (err, results) {
      if (err) {
        res.send("The Narrator pauses. He appears to have run into a problem: "+err);
        return;
      }
      playercount = results.length;
      console.log("\n\nPLAYER COUNT: "+playercount+"\n\n");
  }).then(function(cb) {
      if (playercount >= 16) {
        res.send("The town has too many people in it. You will have to try again some other time.");
        return;
      }
      else {
        var updater = {
            name: '@'+user,
            role: 'VILLAGER',
            isDead: false,
            usedAbility: false
        };
        Player.findOneAndUpdate({name:'@'+user}, updater, {upsert: true}, function (err, doc) {
          if (err) {
            res.send("The Narrator pauses. He appears to have run into a problem: "+err);
            return;
          }
          else {
            if (doc == null) {
              Player.update({name:'@'+user}, updater, {upsert: true}, function(er, docc){});
              res.setHeader('Content-Type', 'application/json');
              res.send(JSON.stringify({"text":"You have joined the town.", "response_type":"in_channel"}));
              return;
            } else {
              res.send("You have already joined the town.");
              return;
            }
          }
        });
      }
  });
});

app.post('/role', function(req, res) {
  var user = req.body['user_name'];
  var channel = req.body['channel_name'];
  if (channel != ACTIVE_CHANNEL) {
    res.send("Please perform this action in #"+ACTIVE_CHANNEL+".");
    return;
  }
  var exists = false;
  Player.findOne({name:'@'+user}).exec(function (err, result) {
      if (err) {
        res.send("The Narrator pauses. He appears to have run into a problem: "+err);
        return;
      }
      if (result != null) {
        exists = true;
      }
  }).then(function(cb){
      console.log("Exists result: "+exists);
      if (!GAME_ACTIVE) {
        res.send("The story has not begun. Your role in it is unclear.");
        return;
      }
      else if (!exists) {
        res.send("You do not appear to be part of this town.");
        return;
      }
      else {
        var role;
        Player.findOne({name:'@'+user}).exec(function (err, result) {
            if (err) {
              res.send("The Narrator pauses. He appears to have run into a problem: "+err);
              return;
            }
            console.log("RESULTS: "+result);
            role = result['role'];
        }).then(function(cb){
            var responsetext = "You are a *"+role+"*.\n\n"
            if (role == 'VILLAGER') {
              responsetext += "You try to keep to yourself mostly. You can vote in town to lynch someone if you so desire. Your goal is to lynch all of the GHOULS in town."
            } else if (role == 'SEER') {
              responsetext += "You are more than just an average VILLAGER, you have the power to reveal someones true nature, but only once per day. You can vote in town to lynch someone if you so desire. Your goal is to lynch all of the GHOULS in town."
            } else if (role == 'GHOUL') {
              responsetext += "You are a filthy GHOUL disguised as a VILLAGER. You can vote in town to lynch someone if you so desire, but you may also collaborate with the other GHOULS to feast on someone every night. Your goal is to ensure the GHOULS outnumber the VILLAGERS."
            } else if (role == 'LICH') {
              responsetext += "You are particularly nasty GHOUL who has mastered prophetic powers and can reveal someones true nature, but only once per day. You can vote in town to lynch someone if you so desire, but you may also collaborate with the other GHOULS to feast on someone every night. Your goal is to ensure the GHOULS outnumber the VILLAGERS."
            } else if (role == 'MASON') {
              responsetext += "You are part of a secret guild with the other MASONS. You know for absolute certain who the other MASONS are and that you can trust them not to be GHOULS. You can check who the other MASONS are at any time. Your goal is to lynch all of the GHOULS in town."
            } else if (role == 'HUNTER') {
              responsetext += "You are a particularly deadly marksman. If anyone wants you dead, they are in for a fight. You can choose another player, who you will slay before being lynched or eaten. Your goal is to lynch all of the GHOULS in town."
            } else if (role == 'CURSED') {
              responsetext += "You were cursed by a powerful LICH at birth. You're still a VILLAGER for the most part but if the GHOULS attempt to kill you, you will turn into a GHOUL yourself. Your goal is to lynch all of the GHOULS in town, or outnumber the VILLAGERS once you have been turned."
            } else if (role == 'PALADIN') {
              responsetext += "You wield a righteous and holy power. You may choose a player other than yourself once per day, and they will be protected from any GHOUL attacks that night. Your goal is to lynch all of the GHOULS in town."
            } else if (role == 'BEHOLDER') {
              responsetext += "You know beyond any doubt who the SEER is. Your goal is to lynch all of the GHOULS in town."
            } else if (role == 'CULTIST') {
              responsetext += "You pass for a VILLAGER but you are only concerned with the growth of your cult. Once a day you may convert any player into your cult, however MASONS will know they have been converted. Your goal is to ensure all living players are part of the cult when the game ends, even if you don't survive yourself."
            }
            res.send(responsetext);
            return;
        });
      }
  });
});

app.post('/votes', function(req, res) {
  var user = req.body['user_name'];
  var channel = req.body['channel_name'];
  if (channel != ACTIVE_CHANNEL) {
    res.send("Please perform this action in #"+ACTIVE_CHANNEL+".");
    return;
  }

  var responsetext = "You open the official town lynching document and begin reading votes.\n\n```"
  Lynch.find().exec(function (err, result) {
      if (err) {
        res.send("The Narrator pauses. He appears to have run into a problem: "+err);
        return;
      }
      if (result.length > 0) {
        for (var i = 0 ; i < result.length ; ++i) {
          if (result[i]["target"] != null) {
            responsetext += result[i]["voter"] + " voted to lynch "+result[i]["target"]+ ".\n";
          } else {
            responsetext += result[i]["voter"] + " voted to lynch nobody.\n";
          }
        }
        responsetext += "```";
        res.send(responsetext);
        return;
      } else {
        res.send("There are currently no active lynch votes.");
        return;
      }
  });
});

app.post('/members', function(req, res) {
  var user = req.body['user_name'];
  var channel = req.body['channel_name'];
  if (channel != ACTIVE_CHANNEL) {
    res.send("Please perform this action in #"+ACTIVE_CHANNEL+".");
    return;
  }

  var responsetext = "You open the official town census and begin reading names to yourself.\n\n```"
  var deadmap = {};
  deadmap[false] = "ALIVE";
  deadmap[true] = "DEAD";
  Player.find().exec(function (err, result) {
      if (err) {
        res.send("The Narrator pauses. He appears to have run into a problem: "+err);
        return;
      }
      if (result.length > 0) {
        for (var i = 0 ; i < result.length ; ++i) {
          responsetext += result[i]["name"] + " ("+deadmap[result[i]["isDead"]] + ")\n";
        }
        responsetext += "```";
        res.send(responsetext);
        return;
      } else {
        res.send("It seems this town is currently empty. Perhaps you'll be the first to join?");
        return;
      }
  });
});

app.post('/ghouls', function(req, res) {
  var user = req.body['user_name'];
  var channel = req.body['channel_name'];
  if (channel != ACTIVE_CHANNEL) {
    res.send("Please perform this action in #"+ACTIVE_CHANNEL+".");
    return;
  } else if (!GAME_ACTIVE) {
    res.send("The story has not begun, there are no GHOULS yet.");
    return;
  }

  Player.findOne({name:'@'+user}).exec(function (err, result) {
      if (err) {
        res.send("The Narrator pauses. He appears to have run into a problem: "+err);
        return;
      }
      // Are they a player?
      if (result == null) {
        res.send("Only ghoulish members of the town may search for their kin.");
        return;
      }
      // Are they not a ghoul?
      else if (result['role'] != 'GHOUL' && result['role'] != 'LICH') {
        res.send("As a non-GHOUL your ability to sense other GHOULS is. . . limited.");
        return;
      }
      // Success is HERE
      else {
        var responsetext = "You skulk around and sense the presence of other ghouls.\n\n```"
        var deadmap = {};
        deadmap[false] = "ALIVE";
        deadmap[true] = "DEAD";
        Player.find({
          'role': { $in: [
              "GHOUL",
              "LICH"
          ]}}).exec(function (err, result) {
            if (err) {
              res.send("The Narrator pauses. He appears to have run into a problem: "+err);
              return;
            }
            if (result.length > 0) {
              for (var i = 0 ; i < result.length ; ++i) {
                responsetext += result[i]["name"] + " (" + result[i]["role"] +  ") ("+deadmap[result[i]["isDead"]] + ")\n";
              }
              responsetext += "```";
              res.send(responsetext);
              return;
            } else {
              res.send("It seems you cannot detect any ghoulish creatures right now. How... odd...");
              return;
            }
        });
      }
  });
});

app.post('/masons', function(req, res) {
  var user = req.body['user_name'];
  var channel = req.body['channel_name'];
  if (channel != ACTIVE_CHANNEL) {
    res.send("Please perform this action in #"+ACTIVE_CHANNEL+".");
    return;
  } else if (!GAME_ACTIVE) {
    res.send("The story has not begun, there are no MASONS yet.");
    return;
  }

  Player.findOne({name:'@'+user}).exec(function (err, result) {
      if (err) {
        res.send("The Narrator pauses. He appears to have run into a problem: "+err);
        return;
      }
      // Are they a player?
      if (result == null) {
        res.send("Only MASONS of the town may search for their guild members.");
        return;
      }
      // Are they not a ghoul?
      else if (result['role'] != 'MASON') {
        res.send("You are not part of the MASONS guild, and do not know who is.");
        return;
      }
      // Success is HERE
      else {
        var responsetext = "You recall the following town members are MASONS.\n\n```"
        var deadmap = {};
        deadmap[false] = "ALIVE";
        deadmap[true] = "DEAD";
        Player.find({
          'role': { $in: [
              "MASON"
          ]}}).exec(function (err, result) {
            if (err) {
              res.send("The Narrator pauses. He appears to have run into a problem: "+err);
              return;
            }
            if (result.length > 0) {
              for (var i = 0 ; i < result.length ; ++i) {
                responsetext += result[i]["name"] + " (" + result[i]["role"] +  ") ("+deadmap[result[i]["isDead"]] + ")\n";
              }
              responsetext += "```";
              res.send(responsetext);
              return;
            } else {
              res.send("It seems you cannot remember any other MASONS right now. Must be a bad headache.");
              return;
            }
        });
      }
  });
});


app.post('/reveal', function(req, res) {
  var text = req.body['text'];
  var user = req.body['user_name'];
  var channel = req.body['channel_name'];
  if (channel != ACTIVE_CHANNEL) {
    res.send("Please perform this action in #"+ACTIVE_CHANNEL+".");
    return;
  } else if (!GAME_ACTIVE) {
    res.send("The story has not begun, it is far too early to do this.");
    return;
  }

  // Verify the person is playing before proceeding.
  Player.findOne({name:'@'+user}).exec(function (err, result) {
      if (err) {
        res.send("The Narrator pauses. He appears to have run into a problem: "+err);
        return;
      }
      // Are they a player?
      if (result == null) {
        res.send("Only members of the town may use this.");
        return;
      }
      // Are they dead?
      else if (result['isDead'] == true) {
        res.send("Unfortunately you are dead.");
        return;
      }
      // Are they not a seer?
      else if (result['role'] != 'SEER' && result['role'] != 'LICH') {
        res.send("You do not possess magical powers.");
        return;
      }
      // Did they do this already?
      else if (result['usedAbility'] == true) {
        res.send("Your prophetic powers are exhausted for today.");
        return;
      }
      // Are they lynching themself?
      else if ('@'+user == text) {
        res.send("You already know what you are . . .");
        return;
      }
      // seeing nobody, then vote abstain.
      else if (text == null || text.replace(/\s/g, '').length < 1) {
        res.send("You do nothing as you did not decide a target for your power.");
        return;
      }
      else {
        // Search for who to see.
        Player.findOne({name:text}).exec(function (err, result) {
            if (err) {
              res.send("The Narrator pauses. He appears to have run into a problem: "+err);
              return;
            }
            // Does the target exist?
            if (result == null) {
              res.send("You focus your powers but cannot find who you are looking for.");
              return;
            // Are they lynching someone who is dead?
            }
            else if (result['isDead'] == true) {
              res.send("No sense in revealing things about the dead.");
              return;
            // Successful Lynch HERE
            }
            else {
              Player.update({name:'@'+user}, {$set: { usedAbility: true }}, {upsert: false}).exec(function (err, rezult) {
                if (err) {
                  console.log(err);
                  return;
                }
                res.send("Focusing your gift, you reveal that "+result['name']+" is a "+result["role"]+"!");
              });
              return;
            }
        });
      }
  });
});

app.post('/lynch', function(req, res) {
  var text = req.body['text'];
  var user = req.body['user_name'];
  var channel = req.body['channel_name'];
  if (channel != ACTIVE_CHANNEL) {
    res.send("Please perform this action in #"+ACTIVE_CHANNEL+".");
    return;
  } else if (!GAME_ACTIVE) {
    res.send("The story has not begun, it is far too early to grab your pitchfork.");
    return;
  }

  // Verify the person is playing before proceeding.
  Player.findOne({name:'@'+user}).exec(function (err, result) {
      if (err) {
        res.send("The Narrator pauses. He appears to have run into a problem: "+err);
        return;
      }
      // Are they a player?
      if (result == null) {
        res.send("Only members of the town may vote to lynch people.");
        return;
      }
      // Are they dead?
      else if (result['isDead'] == true) {
        res.send("Unfortunately the dead do not speak.");
        return;
      }
      // Are they lynching themself?
      else if ('@'+user == text) {
        res.send("In spite of any dark desires, you cannot vote to lynch yourself.");
        return;
      }
      // Lynching nobody, then vote abstain.
      else if (text == null || text.replace(/\s/g, '').length < 1) {
        Lynch.update({voter:'@'+user}, {voter:'@'+user, target:null}, {upsert: true}, function (err, doc) {
          if (err) {
            res.send("The Narrator pauses. He appears to have run into a problem: "+err);
            return;
          }
          else {
            res.setHeader('Content-Type', 'application/json');
            res.send(JSON.stringify({"text":"Quietly, @"+user+" announces they are abstaining from any lynching for now.", "response_type":'in_channel'}));
            return;
          }
        });
      }
      else {
        // Search for who to Lynch.
        Player.findOne({name:text}).exec(function (err, result) {
            if (err) {
              res.send("The Narrator pauses. He appears to have run into a problem: "+err);
              return;
            }
            // Does the target exist?
            if (result == null) {
              res.send("You search the town cautiously but cannot find who you are looking for.");
              return;
            // Are they lynching someone who is dead?
            }
            else if (result['isDead'] == true) {
              res.send("Try as you might, you cannot kill someone who is already dead.");
              return;
            // Successful Lynch HERE
            }
            else {
              Lynch.update({voter:'@'+user}, {voter:'@'+user, target:text}, {upsert: true}, function (err, doc) {
                if (err) {
                  res.send("The Narrator pauses. He appears to have run into a problem: "+err);
                  return;
                }
                else {
                  res.setHeader('Content-Type', 'application/json');
                  res.send(JSON.stringify({"text":"With a shout, @"+user+" announces they vote to lynch "+text+"!", "response_type":'in_channel'}));
                  return;
                }
              });
              return;
            }
        });
      }
  });
});

app.post('/feast', function(req, res) {
  var text = req.body['text'];
  var user = req.body['user_name'];
  var channel = req.body['channel_name'];
  if (channel != ACTIVE_CHANNEL) {
    res.send("Please perform this action in #"+ACTIVE_CHANNEL+".");
    return;
  } else if (!GAME_ACTIVE) {
    res.send("The story has not begun, it is far too early to do this.");
    return;
  }

  // Verify the person is playing before proceeding.
  Player.findOne({name:'@'+user}).exec(function (err, result) {
      if (err) {
        res.send("The Narrator pauses. He appears to have run into a problem: "+err);
        return;
      }
      // Are they a player?
      if (result == null) {
        res.send("Only ghoulish members of the town may feast on people.");
        return;
      }
      // Are they dead?
      else if (result['isDead'] == true) {
        res.send("Unfortunately the dead do not speak.");
        return;
      }
      // Are they not a ghoul based role?
      else if (result['role'] != 'GHOUL' && result['role'] != 'LICH') {
          res.send("You might be hungry, but you have no ghoulish desires for human flesh.");
          return;
      }
      // Are they feasting themself?
      else if ('@'+user == text) {
        res.send("You cannot stand the taste of your own ghoulish flesh.");
        return;
      }
      // Feasting nobody, then vote abstain.
      else if (text == null || text.replace(/\s/g, '').length < 1) {
        Lynch.update({voter:'@'+user}, {voter:'@'+user, target:null}, {upsert: true}, function (err, doc) {
          if (err) {
            res.send("The Narrator pauses. He appears to have run into a problem: "+err);
            return;
          }
          else {
            res.send("In a baffling twist, you decide not to feast on anyone tonight.");
            return;
          }
        });
      }
      else {
        // Search for who to Feast.
        Player.findOne({name:text}).exec(function (err, result) {
            if (err) {
              res.send("The Narrator pauses. He appears to have run into a problem: "+err);
              return;
            }
            // Does the target exist?
            if (result == null) {
              res.send("You search the town cautiously but cannot find who you are looking for.");
              return;
            // Are they feasting someone who is dead?
            }
            else if (result['isDead'] == true) {
              res.send("This poor soul is too dead for even you to eat.");
              return;
            }
            // Are they feasting on a ghoul?
            else if (result['role'] == 'GHOUL' || result['role'] == 'LICH') {
              res.send("You have no taste for the flesh of other fiendish creatures.");
              return;
            }
            // Successful Feast HERE
            else {
              Feast.update({ghoul:'@'+user}, {ghoul:'@'+user, victim:text}, {upsert: true}, function (err, doc) {
                if (err) {
                  res.send("The Narrator pauses. He appears to have run into a problem: "+err);
                  return;
                }
                else {
                  res.send("You decide to make "+text+" your next victim provided the other GHOULS cooperate.");
                  return;
                }
              });
              return;
            }
        });
      }
  });
});

app.post('/startgame', function(req, res) {
  var text = req.body['text'];
  var user = req.body['user_name'];
  var channel = req.body['channel_name'];
  if (channel != ACTIVE_CHANNEL) {
    res.send("Please perform this action in #"+ACTIVE_CHANNEL+".");
    return;
  } else if (GAME_ACTIVE) {
    res.send("The story has begun, no need for this.");
    return;
  } else if (user != 'aedan.burnett') {
    res.send("Only @aedan.burnett may do this.");
  }

  var playercount = 0;
  Player.find().exec(function (err, results) {
      if (err) {
        res.send("The Narrator pauses. He appears to have run into a problem: "+err);
        return;
      }
      playercount = results.length;
      console.log("\n\nPLAYER COUNT: "+playercount+"\n\n");
  }).then(function(cb) {
      if (playercount < 10) {
        res.send("Not enough people have joined the town yet. You need "+parseInt(10-playercount)+" more.");
        return;
      }
  }).then(function(cb){
      res.send("Initial conditions met, beginning to setup story block.");
      Player.aggregate({ $sample: {
          size : 6
        }}).exec(function (err, result) {
          if (err) {
            console.log(err);
            return;
          }
          if (result.length < SPECIALS.length) {
            console.log("Something went wrong with the database query. It didn't select enough people.");
            return;
          }
          /* COMMENTED OUT TEMPORARILY
          for (var i = 0 ; i < result.length ; ++i) {
            Player.update({name:result[i]['name']}, {$set: { role: SPECIALS[i] }}, {upsert: true}).exec(function (err, result) {
              if (err) {
                console.log(err);
                return;
              }
            });
          }*/
          console.log("Finished dishing out roles to players.");
      }).then(function(cb){
          GAME_ACTIVE = true;
          console.log("Game is now active, beginning scheduling block.");
          var lynched;
          var feasted;
          var current_max;
          var tie;
          var ghoulcount;
          var villagercount;
          var storytext;
          var shutdown = false;
          SCHEDULER = schedule.scheduleJob('0 0 * * *', function(){
            console.log('Day completed. Preparing to lynch.');
            storytext = '';
            lynched = null;
            feasted = null;
            current_max = - 1;
            tie = false;
            Lynch.aggregate([
                 {$group : { _id : '$target', count : {$sum : 1}}}
            ]).exec(function (err, result) {
                if (err) {
                  console.log(err);
                  return;
                }
                for (var i = 0; i < result.length; ++i) {
                  if (result[i]["count"] > current_max) {
                    tie = false;
                    lynched = result[i]["_id"];
                    current_max = result[i]["count"];
                  } else if (result[i]["count"] == current_max) {
                    tie = true;
                    lynched = null;
                  }
                }
            }).then(function(cb){
                  console.log("CURRENT MAX "+current_max);
                  Player.findOneAndUpdate({name: lynched}, {isDead : true }, {upsert: false}).exec(function (err, doc) {
                    if (err) {
                      console.log(err);
                      return;
                    }
                    if (lynched != null && current_max > 1) {
                      storytext += "At midnight the bell tolls and "+lynched+" is lynched at the demands of the town.\n";
                      storytext += "Upon their death it is revealed to the town members that they were. . . a "+doc['role']+"!\n";
                      if (doc['role'] != 'GHOUL' && doc['role'] != 'LICH') {
                        storytext += "The town and its populace are filled with grief at the site of this terrible mistake.\n";
                      } else {
                        storytext += "The town and its populace feel vindicated for removing one of these vile creatures from their midst.\n";
                      }
                    } else {
                      storytext += "At midnight the bell tolls but the hangmans noose remains empty as the Town reaches no conclusion about any potential lynching.\n";
                    }
                  }).then(function(cb){
                      Player.count({
                        'role': { $in: [
                            "GHOUL",
                            "LICH"
                        ]},
                        'isDead':false }).exec(function (err, result) {
                        ghoulcount = result;
                        if (ghoulcount <= 0) {
                          shutdown = true;
                          storytext += "Finally all the GHOULS have been slain, and the Town can rest. The VILLAGERS team wins!";
                        }
                      }).then(function(cb){
                          if (shutdown != true) {
                            current_max = -1;
                            tie = false;
                            Feast.aggregate([
                                 {$group : { _id : '$victim', count : {$sum : 1}}}
                            ]).exec(function (err, result) {
                                if (err) {
                                  console.log(err);
                                  return;
                                }
                                for (var i = 0; i < result.length; ++i) {
                                  if (result[i]["count"] > current_max) {
                                    tie = false;
                                    feasted = result[i]["_id"];
                                    current_max = result[i]["count"];
                                  } else if (result[i]["count"] == current_max) {
                                    tie = true;
                                    feasted = null;
                                  }
                                }
                            }).then(function(cb){
                                  console.log("CURRENT MAX "+current_max);
                                  Player.findOneAndUpdate({name: feasted}, {isDead : true }, {upsert: false}).exec(function (err, doc) {
                                    if (err) {
                                      console.log(err);
                                      return;
                                    }
                                    if (feasted != null) {
                                      storytext += "In the morning. . . the body of "+feasted+" is found, half devoured by GHOULS.\nA vile sight to behold. . .\n";
                                    }
                                  }).then(function(cb){
                                      Player.count({
                                        'role': { $in: [
                                            "VILLAGER",
                                            "MASON",
                                            "SEER"
                                        ]},
                                        'isDead':false }).exec(function (err, result) {
                                        playercount = result;
                                        if (ghoulcount >= playercount) {
                                          shutdown = true;
                                          storytext += "The GHOULS begin outnumbering the remaining VILLAGERS and no hope remains for their survival. The GHOULS team wins!";
                                        }
                                      });
                                  });
                              });
                          }
                      });
                  });
            }).then(function(cb){
              setTimeout(function() {
                slackpost(storytext);
                Lynch.remove({}, function(err) {
                 console.log('Lynches removed')
                });
                Feast.remove({}, function(err) {
                 console.log('Feasts removed')
                });
                Player.update({}, {$set: { usedAbility: false }}, {multi : true, upsert: false}, function(er, doc){
                  console.log("Abilities reset.");
                });
                if(shutdown) shutdownGame();
              }, 10000);
            });
          }); //schedule end
      });
  });
});

function shutdownGame() {
  GAME_ACTIVE = false;
  SCHEDULER.cancel();
  Player.remove({}, function(err) {
   console.log('Players removed')
  });
  Lynch.remove({}, function(err) {
   console.log('Lynches removed')
  });
  Feast.remove({}, function(err) {
   console.log('Feasts removed')
  });
  return;
}

function slackpost(text){
  request({
    url: process.env.ANNOUNCEHOOK,
    method: "POST",
    json: true,   // <--Very important!!!
    body: {'text':text}
  }, function (error, response, body){
      console.log(response);
     }
  );
}
