/* NARRATOR BOT WOOOT */

// Global Block
var GAME_ACTIVE = false;
var ACTIVE_CHANNEL = 'the-town';
var MAXPLAYERS = 16;
var MINPLAYERS = 12;

var SPECIALS = ["GHOUL", "GHOUL", "LICH", "SEER", "CURSED", "JESTER", "FOOL", "PALADIN", "HUNTER", "BEHOLDER"];
var SCHEDULER;

var express = require('express');
var request = require('request');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
var moment = require("moment");
var schedule = require("node-schedule");
var app = express();

// These are all defined in a heroku environment variable.
var clientId = process.env.CLIENT_ID;
var clientSecret = process.env.CLIENT_SECRET;
var mongouri = process.env.MONGOLAB_URI;
var db = mongoose.connect(mongouri);

var gameSchema = mongoose.Schema({
    active: {type: Boolean, required: true}
});
var Game = mongoose.model('Game', gameSchema);
Game.findOne({}, 'active', function(e, res){
  GAME_ACTIVE = res['active'];
  console.log('GAME_ACTIVE = '+GAME_ACTIVE);
  if (GAME_ACTIVE) {
    console.log("Beginning game loop.");
    GAMELOOP();
  }
});

var playerSchema = mongoose.Schema({
    name: {type: String, required: true},
    role: {type: String, required: true},
    isDead: {type: Boolean, required: true},
    usedAbility: {type: Boolean, required: true},
    lynched: {type: Boolean, required: true},
    protected: {type: Boolean, required: true},
    marked: {type: Boolean, required: true},
});
var Player = mongoose.model('Player', playerSchema);

var lynchSchema = mongoose.Schema({
    voter: {type: String, required: true},
    target: {type: String},
});
var Lynch = mongoose.model('Lynch', lynchSchema);

var feastSchema = mongoose.Schema({
    ghoul: {type: String, required: true},
    victim: {type: String},
});
var Feast = mongoose.model('Feast', feastSchema);


app.set('port', (process.env.PORT || 5000));

app.use(express.static(__dirname + '/public'));
app.use( bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

// views is directory for all template files
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');

// Simple get, good for testing you launched correctly.
app.get('/', function(request, response) {
  response.send('Narrator Bot Heroku is working! Path Hit: ' + request.url);
});

app.get('/oauth', function(req, res) {
    // When a user authorizes an app, a code query parameter is passed on the oAuth endpoint. If that code is not there, we respond with an error message
    if (!req.query.code) {
        res.status(500);
        res.send({"Error": "Looks like we're not getting code."});
        console.log("Looks like we're not getting code.");
    } else {
        // If it's there...

        // We'll do a GET call to Slack's `oauth.access` endpoint, passing our app's client ID, client secret, and the code we just got as query parameters.
        request({
            url: process.env.OAUTH, //URL to hit
            qs: {code: req.query.code, client_id: clientId, client_secret: clientSecret}, //Query string data
            method: 'GET', //Specify the method

        }, function (error, response, body) {
            if (error) {
                console.log(error);
            } else {
                res.json(body);

            }
        })
    }
});

app.listen(app.get('port'), function() {
  console.log('Node app is running on port', app.get('port'));
});

app.post('/testdata', function(req, res) {
  var user = req.body['user_name'];
  var channel = req.body['channel_name'];
  if (channel != ACTIVE_CHANNEL) {
    res.send("Please perform this action in #"+ACTIVE_CHANNEL+".");
    return;
  }
  if (user != 'aedan.burnett') {
    res.send("Only @aedan.burnett can do this.");
    return;
  }
  if (GAME_ACTIVE == true) {
    res.send("Game has started, don't add test data now.");
    return;
  }
  res.send('Test data populating . . .');
  var dummy;
  for (var i = 0 ; i < 15 ; ++i) {
    var dummy = new Player({name: '@test'+i, role: 'VILLAGER', isDead : false, usedAbility : false, lynched: false, protected: false, marked: false});
    dummy.save({upsert: true}, function (err, item) {
      if (err) return console.error(err);
    });
    Lynch.create({voter:'@test'+i, target:null}, function(e, doccc){});
  }
});

app.post('/commands', function(req, res) {
  res.send('All commands must be used in #'+ACTIVE_CHANNEL+'\n'+
  '```EVERYONE\n'+
  '/townsignup - Join the game.\n'+
  '/townmembers - See the list of players and if they are alive or dead.\n'+
  '/townrole - Learn what role you have been assigned.\n'+
  '/townlynch @victim - Publically vote to lynch @victim. You may abstain by not specifying a victim.\n'+
  '/townvotes - See the list of current lynch votes.\n\n'+

  'GHOULS & LICHS\n'+
  '/townghouls - Privately tells you who the other GHOUL faction players are.\n'+
  '/townfeast @victim - Privately vote to feast on @victim.\n\n'+

  'SEERS & LICHS\n'+
  '/townreveal @target - Privately tells you the role of @target. Limit once per day.\n\n'+

  'HUNTER\n'+
  '/townmark @target - Privately mark a player, who will be killed when you are slain.\n\n'+

  'PALADIN\n'+
  '/townprotect @target - Privately bless a player, who will be protected from GHOULS for the night.\n'+
  '```');
});

app.post('/announce', function(req, res) {
  var text = req.body['text'];
  var user = req.body['user_name'];
  var channel = req.body['channel_name'];
  if (channel != ACTIVE_CHANNEL) {
    res.send("Please perform this action in #"+ACTIVE_CHANNEL+".");
    return;
  }

  // NO ANNOUNCING SHIT UNLESS YOU ARE ME
  if (user != 'aedan.burnett') {
    res.send("You recall that only @aedan.burnett had permission to make announcements as the Narrator.");
    return;
  } else {
    res.send("Preparing announcement. . .");
    slackpost(text);
  }
});

app.post('/signup', function(req, res) {
  var user = req.body['user_name'];
  var channel = req.body['channel_name'];
  if (channel != ACTIVE_CHANNEL) {
    res.send("Please perform this action in #"+ACTIVE_CHANNEL+".");
    return;
  } else if (GAME_ACTIVE) {
    res.send("You regrettably realize the story has begun and you cannot take part. You will have to try again some other time.");
    return;
  }

  var playerCount = 0;
  Player.find().exec(function (err, results) {
      if (err) {
        res.send("The Narrator pauses. He appears to have run into a problem: "+err);
        return;
      }
      playerCount = results.length;
      console.log("\n\nPLAYER COUNT: "+playerCount+"\n\n");
  });
  if (playerCount >= MAXPLAYERS) {
    res.send("The town has too many people in it. You will have to try again some other time.");
    return;
  }
  else {
    var updater = {
        name: '@'+user,
        role: 'VILLAGER',
        isDead: false,
        usedAbility: false,
        lynched: false,
        protected: false,
        marked: false
    };
    Player.findOneAndUpdate({name:'@'+user}, updater, {upsert: true}, function (err, doc) {
      if (err) {
        res.send("The Narrator pauses. He appears to have run into a problem: "+err);
        return;
      }
      else {
        if (doc == null) {
          Player.update({name:'@'+user}, updater, {upsert: true}, function(er, docc){});
          Lynch.create({voter:'@'+user, target:null}, function(e, doccc){});
          res.setHeader('Content-Type', 'application/json');
          res.send(JSON.stringify({"text":"You have joined the town.", "response_type":"in_channel"}));
          return;
        } else {
          res.send("You have already joined the town.");
          return;
        }
      }
    });
  }
});

app.post('/role', function(req, res) {
  var user = req.body['user_name'];
  var channel = req.body['channel_name'];
  if (channel != ACTIVE_CHANNEL) {
    res.send("Please perform this action in #"+ACTIVE_CHANNEL+".");
    return;
  }
  var exists = false;
  Player.findOne({name:'@'+user}).exec(function (err, result) {
      if (err) {
        res.send("The Narrator pauses. He appears to have run into a problem: "+err);
        return;
      }
      if (result != null) {
        exists = true;
      }
  }).then(function(cb){
      console.log("Exists result: "+exists);
      if (!GAME_ACTIVE) {
        res.send("The story has not begun. Your role in it is unclear.");
        return;
      }
      else if (!exists) {
        res.send("You do not appear to be part of this town.");
        return;
      }
      else {
        var role;
        Player.findOne({name:'@'+user}).exec(function (err, result) {
            if (err) {
              res.send("The Narrator pauses. He appears to have run into a problem: "+err);
              return;
            }
            console.log("RESULTS: "+result);
            role = result['role'];
            /* MAKE THE FOOL THINK HE IS THE SEER */
            if (role == 'FOOL') {
              role = 'SEER';
            }
        }).then(function(cb){
            var responsetext = "You are a *"+role+"*.\n\n"
            if (role == 'VILLAGER') {
              responsetext += "You try to keep to yourself mostly. You can vote in town to lynch someone if you so desire. Your goal is to lynch all of the GHOULS in town."
            } else if (role == 'SEER') {
              responsetext += "You are more than just an average VILLAGER, you have the power to reveal someones true nature, but only once per day. You can vote in town to lynch someone if you so desire. Your goal is to lynch all of the GHOULS in town."
            } else if (role == 'GHOUL') {
              responsetext += "You are a filthy GHOUL disguised as a VILLAGER. You can vote in town to lynch someone if you so desire, but you may also collaborate with the other GHOULS to feast on someone every night. Your goal is to ensure the GHOULS outnumber the VILLAGERS."
            } else if (role == 'LICH') {
              responsetext += "You are particularly nasty GHOUL who has mastered prophetic powers and can reveal someones true nature, but only once per day. You can vote in town to lynch someone if you so desire, but you may also collaborate with the other GHOULS to feast on someone every night. Your goal is to ensure the GHOULS outnumber the VILLAGERS."
            } else if (role == 'HUNTER') {
              responsetext += "You are a particularly deadly marksman. If anyone wants you dead, they are in for a fight. You can choose another player, who you will slay before being lynched or eaten. Your goal is to lynch all of the GHOULS in town."
            } else if (role == 'CURSED') {
              responsetext += "You were cursed by a powerful LICH at birth. You're still a VILLAGER for the most part but if the GHOULS attempt to kill you, you will turn into a GHOUL yourself. Your goal is to lynch all of the GHOULS in town, or outnumber the VILLAGERS once you have been turned."
            } else if (role == 'PALADIN') {
              responsetext += "You wield a righteous and holy power. You may choose a player other than yourself once per day, and they will be protected from any GHOUL attacks that night. Your goal is to lynch all of the GHOULS in town."
            } else if (role == 'BEHOLDER') {
              responsetext += "You seem able to sense certain magic. You know beyond any doubt who the SEER is. Your goal is to lynch all of the GHOULS in town."
            } else if (role == 'JESTER') {
              responsetext += "You're a little on the crazy side. Your goal is to get lynched. The game continues after you die, but you are only considered to have won if you were lynched.";
            }
            res.send(responsetext);
            return;
        });
      }
  });
});

app.post('/votes', function(req, res) {
  var user = req.body['user_name'];
  var channel = req.body['channel_name'];
  if (channel != ACTIVE_CHANNEL) {
    res.send("Please perform this action in #"+ACTIVE_CHANNEL+".");
    return;
  }

  var responsetext = "You open the official town lynching document and begin reading votes.\n\n```"
  Lynch.find().exec(function (err, result) {
      if (err) {
        res.send("The Narrator pauses. He appears to have run into a problem: "+err);
        return;
      }
      if (result.length > 0) {
        for (var i = 0 ; i < result.length ; ++i) {
          if (result[i]["target"] != null) {
            responsetext += result[i]["voter"] + " voted to lynch "+result[i]["target"]+ ".\n";
          } else {
            responsetext += result[i]["voter"] + " voted to lynch nobody.\n";
          }
        }
        responsetext += "```";
        res.send(responsetext);
        return;
      } else {
        res.send("There are currently no active lynch votes.");
        return;
      }
  });
});

app.post('/members', function(req, res) {
  var user = req.body['user_name'];
  var channel = req.body['channel_name'];
  if (channel != ACTIVE_CHANNEL) {
    res.send("Please perform this action in #"+ACTIVE_CHANNEL+".");
    return;
  }

  var responsetext = "You open the official town census and begin reading names to yourself.\n\n```"
  var deadmap = {};
  deadmap[false] = "ALIVE";
  deadmap[true] = "DEAD";
  var beholder = false;
  Player.findOne({name: '@'+user}).exec(function (err, rezult){
    if (err || rezult == null) {
      err ? "No town members" : err;
      res.send("The Narrator pauses. He appears to have run into a problem: "+err);
      return;
    }
    if (rezult['role'] == 'BEHOLDER') {
      beholder = true;
    }
    Player.find().exec(function (err, result) {
        if (err) {
          res.send("The Narrator pauses. He appears to have run into a problem: "+err);
          return;
        }
        if (result.length > 0) {
          for (var i = 0 ; i < result.length ; ++i) {
            // Lynched people are revealed.
            if (result[i]["lynched"] == true) {
              responsetext += result[i]["name"] + " ("+deadmap[result[i]["isDead"]] + ") ("+result[i]["role"]+")\n"
            // Show the BEHOLDER the SEER
            } else if (beholder && result[i]["role"] == 'SEER'){
              responsetext += result[i]["name"] + " ("+deadmap[result[i]["isDead"]] + ") ("+result[i]["role"]+")\n";
            } else {
              responsetext += result[i]["name"] + " ("+deadmap[result[i]["isDead"]] + ")\n";
            }
          }
          responsetext += "```";
          res.send(responsetext);
          return;
        } else {
          res.send("It seems this town is currently empty. Perhaps you'll be the first to join?");
          return;
        }
    });
  });
});

app.post('/ghouls', function(req, res) {
  var user = req.body['user_name'];
  var channel = req.body['channel_name'];
  if (channel != ACTIVE_CHANNEL) {
    res.send("Please perform this action in #"+ACTIVE_CHANNEL+".");
    return;
  } else if (!GAME_ACTIVE) {
    res.send("The story has not begun, there are no GHOULS yet.");
    return;
  }

  Player.findOne({name:'@'+user}).exec(function (err, result) {
      if (err) {
        res.send("The Narrator pauses. He appears to have run into a problem: "+err);
        return;
      }
      // Are they a player?
      if (result == null) {
        res.send("Only ghoulish members of the town may search for their kin.");
        return;
      }
      // Are they not a ghoul?
      else if (result['role'] != 'GHOUL' && result['role'] != 'LICH') {
        res.send("As a non-GHOUL your ability to sense other GHOULS is. . . limited.");
        return;
      }
      // Success is HERE
      else {
        var responsetext = "You skulk around and sense the presence of other ghouls.\n\n```"
        var deadmap = {};
        deadmap[false] = "ALIVE";
        deadmap[true] = "DEAD";
        Player.find({
          'role': { $in: [
              "GHOUL",
              "LICH"
          ]}}).exec(function (err, result) {
            if (err) {
              res.send("The Narrator pauses. He appears to have run into a problem: "+err);
              return;
            }
            if (result.length > 0) {
              for (var i = 0 ; i < result.length ; ++i) {
                responsetext += result[i]["name"] + " (" + result[i]["role"] +  ") ("+deadmap[result[i]["isDead"]] + ")\n";
              }
              responsetext += "```";
              res.send(responsetext);
              return;
            } else {
              res.send("It seems you cannot detect any ghoulish creatures right now. How... odd...");
              return;
            }
        });
      }
  });
});

app.post('/reveal', function(req, res) {
  var text = req.body['text'];
  var user = req.body['user_name'];
  var channel = req.body['channel_name'];
  if (channel != ACTIVE_CHANNEL) {
    res.send("Please perform this action in #"+ACTIVE_CHANNEL+".");
    return;
  } else if (!GAME_ACTIVE) {
    res.send("The story has not begun, it is far too early to do this.");
    return;
  }

  var fool = false;
  // Verify the person is playing before proceeding.
  Player.findOne({name:'@'+user}).exec(function (err, result) {
      if (err) {
        res.send("The Narrator pauses. He appears to have run into a problem: "+err);
        return;
      }
      // Are they a player?
      if (result == null) {
        res.send("Only members of the town may use this.");
        return;
      }
      // Are they dead?
      else if (result['isDead'] == true) {
        res.send("Unfortunately you are dead.");
        return;
      }
      // Are they not a seer?
      else if (result['role'] != 'SEER' && result['role'] != 'LICH' && result['role'] != 'FOOL') {
        res.send("You do not possess magical powers.");
        return;
      }
      // Did they do this already?
      else if (result['usedAbility'] == true) {
        res.send("Your prophetic powers are exhausted for today.");
        return;
      }
      // Are they lynching themself?
      else if ('@'+user == text) {
        res.send("You already know what you are . . .");
        return;
      }
      // seeing nobody, then vote abstain.
      else if (text == null || text.replace(/\s/g, '').length < 1) {
        res.send("You do nothing as you did not decide a target for your power.");
        return;
      }
      else {
        // Before we continue, check if they were a FOOL.
        if (result['role'] == 'FOOL') {
          fool = true;
        }
        // Search for who to see.
        Player.findOne({name:text}).exec(function (err, result) {
            if (err) {
              res.send("The Narrator pauses. He appears to have run into a problem: "+err);
              return;
            }
            // Does the target exist?
            if (result == null) {
              res.send("You focus your powers but cannot find who you are looking for.");
              return;
            // Are they revealing someone who is dead?
            }
            else if (result['isDead'] == true) {
              res.send("No sense in revealing things about the dead.");
              return;
            // Successful reveal HERE
            }
            else {
              Player.update({name:'@'+user}, {$set: { usedAbility: true }}, {upsert: false}).exec(function (err, rezult) {
                if (err) {
                  console.log(err);
                  return;
                }
                if (!fool) {
                  res.send("Focusing your gift, you reveal that "+result['name']+" is a "+result["role"]+"!");
                } else {
                  // HAH. Get rekt FOOL.
                  var rolearray = SPECIALS.concat(["VILLAGER","VILLAGER","VILLAGER","VILLAGER"]);
                  res.send("Focusing your gift, you reveal that "+result['name']+" is a "+rolearray[Math.floor(Math.random()*rolearray.length)]+"!");
                }
                return;
              });
            }
        });
      }
  });
});

app.post('/protect', function(req, res) {
  var text = req.body['text'];
  var user = req.body['user_name'];
  var channel = req.body['channel_name'];
  if (channel != ACTIVE_CHANNEL) {
    res.send("Please perform this action in #"+ACTIVE_CHANNEL+".");
    return;
  } else if (!GAME_ACTIVE) {
    res.send("The story has not begun, it is far too early to do this.");
    return;
  }

  // Verify the person is playing before proceeding.
  Player.findOne({name:'@'+user}).exec(function (err, result) {
      if (err) {
        res.send("The Narrator pauses. He appears to have run into a problem: "+err);
        return;
      }
      // Are they a player?
      if (result == null) {
        res.send("Only members of the town may use this.");
        return;
      }
      // Are they dead?
      else if (result['isDead'] == true) {
        res.send("Unfortunately you are dead.");
        return;
      }
      // Are they not a paladin?
      else if (result['role'] != 'PALADIN') {
        res.send("You do not possess holy powers.");
        return;
      }
      // Did they do this already?
      else if (result['usedAbility'] == true) {
        res.send("Your holy powers are exhausted for today.");
        return;
      }
      // Are they blessing themself?
      else if ('@'+user == text) {
        res.send("You may not provide this blessing to yourself.");
        return;
      }
      // seeing nobody, then vote abstain.
      else if (text == null || text.replace(/\s/g, '').length < 1) {
        res.send("You must specify who to protect.");
        return;
      }
      else {
        // Search for who to bless.
        Player.findOne({name:text}).exec(function (err, result) {
            if (err) {
              res.send("The Narrator pauses. He appears to have run into a problem: "+err);
              return;
            }
            // Does the target exist?
            if (result == null) {
              res.send("You focus your powers but cannot find who you are looking for.");
              return;
            // Are they protecting someone who is dead?
            }
            else if (result['isDead'] == true) {
              res.send("No sense in blessing the dead.");
              return;
            // Successful protect HERE
            }
            else {
              Player.update({name:'@'+user}, {$set: { usedAbility: true }}, {upsert: false}).exec(function (err, rezult) {
                if (err) {
                  console.log(err);
                  return;
                }

                // PROTECT THAT PERSON.
                Player.update({name:text}, {$set: { protected: true }}, {upsert: false}).exec(function (err, rezult) {
                  if (err) {
                    console.log(err);
                    return;
                  }
                  res.send("Focusing your gift, you provide a protective blessing to "+result['name']+"!");
                  return;
                });
              });
            }
        });
      }
  });
});

app.post('/mark', function(req, res) {
  var text = req.body['text'];
  var user = req.body['user_name'];
  var channel = req.body['channel_name'];
  if (channel != ACTIVE_CHANNEL) {
    res.send("Please perform this action in #"+ACTIVE_CHANNEL+".");
    return;
  } else if (!GAME_ACTIVE) {
    res.send("The story has not begun, it is far too early to do this.");
    return;
  }

  // Verify the person is playing before proceeding.
  Player.findOne({name:'@'+user}).exec(function (err, result) {
      if (err) {
        res.send("The Narrator pauses. He appears to have run into a problem: "+err);
        return;
      }
      // Are they a player?
      if (result == null) {
        res.send("Only members of the town may use this.");
        return;
      }
      // Are they dead?
      else if (result['isDead'] == true) {
        res.send("Unfortunately you are dead.");
        return;
      }
      // Are they not a hunter?
      else if (result['role'] != 'HUNTER') {
        res.send("You do not possess strong marksmanship.");
        return;
      }
      // Are they marking themself?
      else if ('@'+user == text) {
        res.send("You may not mark yourself.");
        return;
      }
      // seeing nobody, then vote abstain.
      else if (text == null || text.replace(/\s/g, '').length < 1) {
        Player.update({}, {$set: { marked: false }}, {multi: true, upsert: false}).exec(function (err, rezult) {
          if (err) {
            console.log(err);
            return;
          }
          res.send("You decide not to bring anyone down with you.");
          return;
        });
      }
      else {
        // Search for who to mark.
        Player.findOne({name:text}).exec(function (err, result) {
            if (err) {
              res.send("The Narrator pauses. He appears to have run into a problem: "+err);
              return;
            }
            // Does the target exist?
            if (result == null) {
              res.send("You cannot seem to find your target.");
              return;
            // Are they marking someone who is dead?
            }
            else if (result['isDead'] == true) {
              res.send("You cannnot kill what is already dead.");
              return;
            // Successful marking HERE
            }
            else {
              // CLEAR ALL MARKS.
              Player.update({}, {$set: { marked: false }}, {multi: true, upsert: false}).exec(function (err, rezult) {
                if (err) {
                  console.log(err);
                  return;
                }

                // MARK SOMEONE
                Player.update({name:text}, {$set: { marked: true }}, {upsert: false}).exec(function (err, rezult) {
                  if (err) {
                    console.log(err);
                    return;
                  }
                  res.send("You set your sights, and mark "+result['name']+" for death.");
                  return;
                });
              });
            }
        });
      }
  });
});

app.post('/lynch', function(req, res) {
  var text = req.body['text'];
  var user = req.body['user_name'];
  var channel = req.body['channel_name'];
  if (channel != ACTIVE_CHANNEL) {
    res.send("Please perform this action in #"+ACTIVE_CHANNEL+".");
    return;
  } else if (!GAME_ACTIVE) {
    res.send("The story has not begun, it is far too early to grab your pitchfork.");
    return;
  }

  // Verify the person is playing before proceeding.
  Player.findOne({name:'@'+user}).exec(function (err, result) {
      if (err) {
        res.send("The Narrator pauses. He appears to have run into a problem: "+err);
        return;
      }
      // Are they a player?
      if (result == null) {
        res.send("Only members of the town may vote to lynch people.");
        return;
      }
      // Are they dead?
      else if (result['isDead'] == true) {
        res.send("Unfortunately the dead do not speak.");
        return;
      }
      // Are they lynching themself?
      else if ('@'+user == text) {
        res.send("In spite of any dark desires, you cannot vote to lynch yourself.");
        return;
      }
      // Lynching nobody, then vote abstain.
      else if (text == null || text.replace(/\s/g, '').length < 1) {
        Lynch.update({voter:'@'+user}, {voter:'@'+user, target:null}, {upsert: true}, function (err, doc) {
          if (err) {
            res.send("The Narrator pauses. He appears to have run into a problem: "+err);
            return;
          }
          else {
            res.setHeader('Content-Type', 'application/json');
            res.send(JSON.stringify({"text":"Quietly, @"+user+" announces they are abstaining from any lynching for now.", "response_type":'in_channel'}));
            return;
          }
        });
      }
      else {
        // Search for who to Lynch.
        Player.findOne({name:text}).exec(function (err, result) {
            if (err) {
              res.send("The Narrator pauses. He appears to have run into a problem: "+err);
              return;
            }
            // Does the target exist?
            if (result == null) {
              res.send("You search the town cautiously but cannot find who you are looking for.");
              return;
            // Are they lynching someone who is dead?
            }
            else if (result['isDead'] == true) {
              res.send("Try as you might, you cannot kill someone who is already dead.");
              return;
            // Successful Lynch HERE
            }
            else {
              Lynch.update({voter:'@'+user}, {voter:'@'+user, target:text}, {upsert: true}, function (err, doc) {
                if (err) {
                  res.send("The Narrator pauses. He appears to have run into a problem: "+err);
                  return;
                }
                else {
                  res.setHeader('Content-Type', 'application/json');
                  res.send(JSON.stringify({"text":"With a shout, @"+user+" announces they vote to lynch "+text+"!", "response_type":'in_channel'}));
                  return;
                }
              });
              return;
            }
        });
      }
  });
});

app.post('/feast', function(req, res) {
  var text = req.body['text'];
  var user = req.body['user_name'];
  var channel = req.body['channel_name'];
  if (channel != ACTIVE_CHANNEL) {
    res.send("Please perform this action in #"+ACTIVE_CHANNEL+".");
    return;
  } else if (!GAME_ACTIVE) {
    res.send("The story has not begun, it is far too early to do this.");
    return;
  }

  // Verify the person is playing before proceeding.
  Player.findOne({name:'@'+user}).exec(function (err, result) {
      if (err) {
        res.send("The Narrator pauses. He appears to have run into a problem: "+err);
        return;
      }
      // Are they a player?
      if (result == null) {
        res.send("Only ghoulish members of the town may feast on people.");
        return;
      }
      // Are they dead?
      else if (result['isDead'] == true) {
        res.send("Unfortunately the dead do not speak.");
        return;
      }
      // Are they not a ghoul based role?
      else if (result['role'] != 'GHOUL' && result['role'] != 'LICH') {
          res.send("You might be hungry, but you have no ghoulish desires for human flesh.");
          return;
      }
      // Are they feasting themself?
      else if ('@'+user == text) {
        res.send("You cannot stand the taste of your own ghoulish flesh.");
        return;
      }
      // Feasting nobody, then vote abstain.
      else if (text == null || text.replace(/\s/g, '').length < 1) {
        Lynch.update({voter:'@'+user}, {voter:'@'+user, target:null}, {upsert: true}, function (err, doc) {
          if (err) {
            res.send("The Narrator pauses. He appears to have run into a problem: "+err);
            return;
          }
          else {
            res.send("In a baffling twist, you decide not to feast on anyone tonight.");
            return;
          }
        });
      }
      else {
        // Search for who to Feast.
        Player.findOne({name:text}).exec(function (err, result) {
            if (err) {
              res.send("The Narrator pauses. He appears to have run into a problem: "+err);
              return;
            }
            // Does the target exist?
            if (result == null) {
              res.send("You search the town cautiously but cannot find who you are looking for.");
              return;
            // Are they feasting someone who is dead?
            }
            else if (result['isDead'] == true) {
              res.send("This poor soul is too dead for even you to eat.");
              return;
            }
            // Are they feasting on a ghoul?
            else if (result['role'] == 'GHOUL' || result['role'] == 'LICH') {
              res.send("You have no taste for the flesh of other fiendish creatures.");
              return;
            }
            // Successful Feast HERE
            else {
              Feast.update({ghoul:'@'+user}, {ghoul:'@'+user, victim:text}, {upsert: true}, function (err, doc) {
                if (err) {
                  res.send("The Narrator pauses. He appears to have run into a problem: "+err);
                  return;
                }
                else {
                  res.send("You decide to make "+text+" your next victim provided the other GHOULS cooperate.");
                  return;
                }
              });
              return;
            }
        });
      }
  });
});

app.post('/stopgame', function(req, res) {
  var text = req.body['text'];
  var user = req.body['user_name'];
  var channel = req.body['channel_name'];
  if (channel != ACTIVE_CHANNEL) {
    res.send("Please perform this action in #"+ACTIVE_CHANNEL+".");
    return;
  } else if (!GAME_ACTIVE) {
    res.send("Game is not active.");
    return;
  } else if (user != 'aedan.burnett') {
    res.send("Only @aedan.burnett may do this.");
  }
  Game.findOneAndUpdate({}, {active : false}, {upsert: true}, function (err, doc) {
    console.log("GAME_ACTIVE = false");
    GAME_ACTIVE = false;
    res.send("Game deactivated. Note: Scheduling block may still run. Reboot server.");
    return;
  });
})

app.post('/startgame', function(req, res) {
  var text = req.body['text'];
  var user = req.body['user_name'];
  var channel = req.body['channel_name'];
  if (channel != ACTIVE_CHANNEL) {
    res.send("Please perform this action in #"+ACTIVE_CHANNEL+".");
    return;
  } else if (GAME_ACTIVE) {
    res.send("The story has begun, no need for this.");
    return;
  } else if (user != 'aedan.burnett') {
    res.send("Only @aedan.burnett may do this.");
  }

  res.send("Attempting game loop.");
  GAMELOOP();
});

function GAMELOOP() {

  /* INITIAL CONDITIONS BLOCK : CAN WE START? IF SO, ASSIGN ROLES AND BEGIN GAME */
  var wasGameActiveAlready = false;
  Player.find().exec(function (err, results) {
      if (err) {
        console.log(err);
        return;
      }
      console.log("\n\nPLAYER COUNT: "+results.length+"\n\n");
      if (results.length >= MINPLAYERS && results.length <= MAXPLAYERS) {

        Game.findOne({}, function (err, doc) {
          wasGameActiveAlready = doc['active'];
          /* Only assign roles if the GAME_ACTIVE wasnt initiall active */
          if (wasGameActiveAlready == false) {
            Player.aggregate({ $sample: {
                size : SPECIALS.length
              }}).exec(function (err, result) {
                if (err) {
                  console.log(err);
                  return;
                }
                if (result.length != SPECIALS.length) {
                  console.log("Something went wrong with the database query. It didn't select the correct amount people.");
                  return;
                }
                for (var i = 0 ; i < result.length ; ++i) {
                  Player.update({name:result[i]['name']}, {$set: { role: SPECIALS[i] }}, {upsert: true}).exec(function (err, result) {
                    if (err) {
                      console.log(err);
                      return;
                    }
                  });
                }
                console.log("Finished dishing out roles to players.");
            });
          }

          // Set game to active if it wasn't already.
          doc['active'] = true;
          doc.save();
          console.log("GAME_ACTIVE = true");
          GAME_ACTIVE = true;
        });
        return true;
      } else {
        console.log("Error incorrect amount of players.");
        return false;
      }
  });

  /* WAIT LIKE 10 SECONDS - JUST IN CASE */
  setTimeout(function(){
    SCHEDULER = schedule.scheduleJob('0 0 * * 1-5', function(){
    var playerData;
    var lynchData;
    var feastData;

    Player.find({}, 'name role isDead usedAbility lynched protected marked', {multi: true}, function(err){
      console.log("Obtained player data.");
    }).then(function(cb){
      playerData = cb;
      ld = Lynch.find({}, 'voter target', { multi: true}, function(err){
        console.log("Obtained lynch data.");
      });
      return ld;
    }).then(function(cb){
      lynchData = cb;
      fd = Feast.find({}, 'ghoul victim', { multi: true}, function(err){
        console.log("Obtained feast data.");
      });
      return fd;
    /* All data has now been obtained. */
    }).then(function(cb){
      feastData = cb;
      console.log("Player: "+JSON.stringify(playerData));
      console.log("Lynch: "+JSON.stringify(lynchData));
      console.log("Feast: "+JSON.stringify(feastData));
      return;
    /* GOT DATA NOW BEGIN */
    }).then(function(cb){

      console.log('Day completed. Preparing to lynch.');
      storytext = '';
      lynchedPerson = null;
      feastedPerson = null;
      markedPerson = null;
      currentMax = - 1;
      tie = false;
      ghoulCount = 0;
      villagerCount = 0;

      /* Count out the lynch votes */
      var lynchMap = {};
      for (var i = 0; i < lynchData.length; ++i) {
        if (lynchData[i]['target'] != null) {
          if (lynchMap[lynchData[i]['target']] === undefined) {
            lynchMap[lynchData[i]['target']] = 1;
          } else {
            lynchMap[lynchData[i]['target']] += 1;
          }
        }
      }
      /* Loop through lynch votes, determine who gets lynched. */
      for (var key in lynchMap) {
        if (lynchMap.hasOwnProperty(key)) {
          if (lynchMap[key] > currentMax) {
            tie = false;
            lynchedPerson = key;
            currentMax = lynchMap[key];
          } else if (lynchMap[key] == currentMax) {
            tie = true;
            lynchedPerson = null;
          }
        }
      }
      console.log("LYNCHED PERSON: "+lynchedPerson+"\nCURRENT_MAX: "+currentMax);
      /* If an eligible lynching happens, update their document. */
      for (var i in playerData) {
        if (playerData[i]['name'] == lynchedPerson) {
          storytext += 'At midnight the bell tolls and '+lynchedPerson+' is lynched at the demands of the town.\n';
          storytext += 'Upon their death it is revealed that '+lynchedPerson+' was . . . a '+playerData[i]['role']+'!\n\n';
          if (playerData[i]['role'] == 'JESTER') {
            storytext += 'The people seem uneasy, but they can tell by the incessant cackling that '+playerData[i]['name']+' got EXACTLY what they wanted.\n\n';
          } else if (playerData[i]['role'] == 'CURSED') {
            storytext += 'The people are unhappy, but they take solace in knowing they prevented the creation of another potential GHOUL.\n\n';
          } else if (playerData[i]['role'] == 'LICH') {
            storytext += 'The people cheer with relief for having slain the leader of the vile GHOULS.\n\n';
          } else if (playerData[i]['role'] == 'GHOUL') {
            storytext += 'The people sigh with relief for having slain this vile creature.\n\n';
          } else if (playerData[i]['role'] == 'PALADIN') {
            storytext += 'Devastated, the people construct '+playerData[i]['name']+' a tomb. How can they hope to defend against the GHOULS now?!\n\n';
          } else if (playerData[i]['role'] == 'FOOL') {
            storytext += 'The people are certainly unhappy to lynch one of their own, but at least the confusing lies will come to an end.\n\n';
          } else if (playerData[i]['role'] == 'SEER') {
            storytext += 'The people are wrought with guilt. If only they had foreseen this disastrous misjudgement.\n\n';
          } else if (playerData[i]['role'] == 'HUNTER') {
            storytext += 'The people are stricken with remorse, but the executioner noted that the victim had an unusually empty quiver.\n\n';
          } else if (playerData[i]['role'] == 'BEHOLDER') {
            storytext += 'The people whisper eachother with feelings of fear, superstition and hopelessness.\n\n';
          } else {
            storytext += 'The people feel a deep sense of regret for their hasty mistake.\n\n';
          }
          playerData[i]['isDead'] = true;
          playerData[i]['lynched'] = true;
        }
      }

      /* check if any ghouls are left, if there are not any left we can stop everything now */
      ghoulCount = 0;
      for (var i in playerData) {
        if (playerData[i]['isDead'] == false) {
          if (playerData[i]['role'] == 'GHOUL' || playerData[i]['role'] == 'LICH') {
            ghoulCount += 1;
          }
        }
      }
      if (ghoulCount <= 0) {
        storytext += 'Now that the vile horde of ghoulish monsters has been purged from The Town, the people are finally safe!\n\n';
        slackpost(storytext);
        shutdownGame();
        return;
      }

      /* Count out the feast votes */
      currentMax = -1;  // re-use this var
      var feastMap = {};
      for (var i = 0; i < feastData.length; ++i) {
        if (feastData[i]['victim'] != null) {
          if (feastMap[feastData[i]['victim']] === undefined) {
            feastMap[feastData[i]['victim']] = 1;
          } else {
            feastMap[feastData[i]['victim']] += 1;
          }
        }
      }
      /* Loop through feast votes, determine who gets feasted. */
      for (var key in feastMap) {
        if (feastMap.hasOwnProperty(key)) {
          if (feastMap[key] > currentMax) {
            tie = false;
            feastedPerson = key;
            currentMax = feastMap[key];
          } else if (feastMap[key] == currentMax) {
            tie = true;
            feastedPerson = null;
          }
        }
      }
      console.log("FEASTED PERSON: "+feastedPerson+"\nCURRENT_MAX: "+currentMax);

      /* If an eligible feasting happens, update their document. */
      if (feastedPerson != lynchedPerson) {
        for (var i in playerData) {
          if (playerData[i]['name'] == feastedPerson && playerData[i]['protected'] == true){
            storytext += "In the night the GHOULS attempted to devour someone, but a shimmering holy light repelled them from consuming their desired meal.\n\n";
          } else if (playerData[i]['name'] == feastedPerson && playerData[i]['role'] != 'CURSED') {
            storytext += "In the morning. . . the body of "+feastedPerson+" is found, mauled by GHOULS.\n\n";
            playerData[i]['isDead'] = true;
          } else if (playerData[i]['name'] == feastedPerson && playerData[i]['role'] == 'CURSED') {
            storytext += "In the night the GHOULS discovered a CURSED villager and transformed them into another GHOUL.\n\n";
            playerData[i]['role'] = 'GHOUL';
          }
        }
      }

      /* Find the marked target, should never be more than one. */
      var shotsfired = false;
      for (var i = 0; i < playerData.length; ++i) {
        // This check ensures the Hunter died this night.
        if (playerData[i]['role'] == 'HUNTER'
            && playerData[i]['isDead'] == true
            && (playerData[i]['name'] == lynchedPerson
            ||  playerData[i]['name'] == feastedPerson)) {
              shotsfired = true;
        }
        if (playerData[i]['marked'] == true) {
          markedPerson = playerData[i]['name'];
        }
      }

      /* If an eligible hunting happens, update their document. */
      if (markedPerson != lynchedPerson && markedPerson != feastedPerson && shotsfired) {
        for (var i in playerData) {
          if (playerData[i]['name'] == markedPerson) {
            storytext += "During the commotion of the nights events, an arrow finds its mark in the chest of "+markedPerson+" killing them on the spot.\n\n";
            playerData[i]['isDead'] = true;
          }
        }
      }

      /* Check the state of things; there are a few possible out comes:
      1. Ghouls now match or outnumber villagers.
      2. Ghouls are all dead.
      3. Ghouls and villagers are all dead.
      4. Ghouls and villagers are both still in the game. */
      ghoulCount = 0;
      villagerCount = 0;
      for (var i in playerData) {
        if (playerData[i]['isDead'] == false) {
          if (playerData[i]['role'] == 'GHOUL' || playerData[i]['role'] == 'LICH') {
            ghoulCount += 1;
          } else {
            villagerCount += 1;
          }
        }
      }
      if (ghoulCount <= 0 && villagerCount <= 0) {
        storytext += 'A cold wind blows. The Town. . . is empty. None who live remain. Truly a desolate ending.\n\n';
        slackpost(storytext);
        shutdownGame();
        return;
      } else if (ghoulCount >= villagerCount) {
        storytext += 'The fiendish GHOULS begin to overpower the villagers and proceed to take over The Town.\n\n';
        slackpost(storytext);
        shutdownGame();
        return;
      } else if (ghoulCount <= 0) {
        storytext += 'Now that the vile horde of ghoulish monsters has been purged from The Town, the people are finally safe!\n\n';
        slackpost(storytext);
        shutdownGame();
        return;
      }

      /* Reset powers, lynching votes, ghoul votes, protection etc */
      for (var i = 0; i < playerData.length; ++i) {
        playerData[i]['usedAbility'] = false;
        playerData[i]['protected'] = false;
        playerData[i]['marked'] = false;
      }
      for (var i = 0; i < lynchData.length; ++i) {
        lynchData[i]['target'] = null;
      }
      for (var i = 0; i < feastData.length; ++i) {
        feastData[i]['victim'] = null;
      }

      /* Commit all the updated documents to the database. */
      for (var i = 0; i < playerData.length; ++i) {
        /* As a note; it does not matter if these are synchronous. */
        Player.update({name:playerData[i]['name']}, {$set: {
          role: playerData[i]['role'],
          isDead: playerData[i]['isDead'],
          usedAbility: playerData[i]['usedAbility'],
          lynched: playerData[i]['lynched'],
          protected: playerData[i]['protected'],
          marked: playerData[i]['marked']
        }}, {upsert: false}).exec(function (err, result) {
          if (err) console.log(err);
        });
      }
      for (var i = 0; i < lynchData.length; ++i) {
        Lynch.update({voter:lynchData[i]['voter']}, {$set: {
          target: null
        }}, {upsert: false}).exec(function (err, result) {
          if (err) console.log(err);
        });
      }
      for (var i = 0; i < feastData.length; ++i) {
        Feast.update({ghoul:feastData[i]['ghoul']}, {$set: {
          victim: null
        }}, {upsert: false}).exec(function (err, result) {
          if (err) console.log(err);
        });
      }

      /* Print the story text to slack */
      if (storytext == '') {
        storytext = "In a complete surprise to all residents, nothing of note happened in The Town tonight.";
      }
      slackpost(storytext);
      console.log(storytext);
    });
  });
  }, 10000);
}

function shutdownGame() {
  Game.findOneAndUpdate({}, {active : false}, {upsert: true}, function (err, doc) {
    console.log("GAME_ACTIVE = false");
    GAME_ACTIVE = false;
  });
  //return; //TEMPORARY
  SCHEDULER.cancel();
  Player.remove({}, function(err) {
   console.log('Players removed')
  });
  Lynch.remove({}, function(err) {
   console.log('Lynches removed')
  });
  Feast.remove({}, function(err) {
   console.log('Feasts removed')
  });
  return;
}

function slackpost(text){
  request({
    url: process.env.ANNOUNCEHOOK,
    method: "POST",
    json: true,   // <--Very important!!!
    body: {'text':text}
  }, function (error, response, body){
      console.log(response);
     }
  );
}
